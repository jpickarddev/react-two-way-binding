import React from 'react';
import './App.css';
import OuterForm from './components/OuterForm';

function App() {
  return (
    <div className="App">
      <OuterForm></OuterForm>
    </div>
  );
}

export default App;
