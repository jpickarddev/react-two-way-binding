import React from 'react'
const InnerForm = ({ displayText, handleTextChange, handleClearOnClick }) => {

    return (
        <div>
            <h2>Inner Form</h2>
            <label>Inner Text </label>
            <input type="text" value={displayText} onChange={handleTextChange}></input>
            <button id="clearButton" onClick={handleClearOnClick}>Clear</button>
        </div>
    );
}
export default InnerForm;