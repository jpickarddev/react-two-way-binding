import React, { useState } from 'react';
import InnerForm from './InnerForm';

const OuterForm = () => {

    const [OuterText, setOuterText] = useState("");

    const handleTextChange = (e) => {
        setOuterText(e.target.value);
    }
    const handleClearButtonOnClick = () => {
        setOuterText("");
    }


    return (
        <div>
            <h1>Outer Form</h1>
            <label>Outer Text </label>
            <input type="text" value={OuterText} onChange={handleTextChange}></input>
            <InnerForm displayText={OuterText} handleTextChange={handleTextChange} handleClearOnClick={handleClearButtonOnClick}></InnerForm>
        </div>
    );
}
export default OuterForm;